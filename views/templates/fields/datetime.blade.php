<!-- Field: {{ strtoupper($fieldName) }} -->
@@php
    ${{ $fieldName }}_config = ['format' => 'YYYY-MM-DD HH:mm'];
@@endphp
@if($config->options->localized)
<{{ "x-adminlte-input-date" }} fgroup-class="col-sm-6" enable-old-support
    name="{{ $fieldName }}"
    label="@{!! __('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}') !!}:"
    placeholder="@{!! __('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}') !!}"
    :config="${{ $fieldName }}_config"
    :value="old('{{ $fieldName }}', isset(${{ $config->modelNames->camel }}) ? optional(${{ $config->modelNames->camel }}->{{ $fieldName }})->format('Y-m-d H:i') : null)"
    />
@else
<{{ "x-adminlte-input-date" }} fgroup-class="col-sm-6" enable-old-support
    name="{{ $fieldName }}"
    label="{{ $fieldTitle }}:"
    placeholder="{{ $fieldTitle }}"
    :config="${{ $fieldName }}_config"
    :value="old('{{ $fieldName }}', isset(${{ $config->modelNames->camel }}) ? optional(${{ $config->modelNames->camel }}->{{ $fieldName }})->format('Y-m-d H:i') : null)"
    />
@endif