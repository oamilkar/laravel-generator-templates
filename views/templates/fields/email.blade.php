<!-- Field: {{ strtoupper($fieldName) }} -->
@if($config->options->localized)
<{{ "x-adminlte-input" }} fgroup-class="col-sm-6" enable-old-support
    type="email"
    name="{{ $fieldName }}"
    label="@{!! __('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}') !!}:"
    placeholder="@{!! __('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}') !!}"
    :value="old('{{ $fieldName }}', optional(${{ $config->modelNames->camel }} ?? null)->{{ $fieldName }})"
    />
@else
<{{ "x-adminlte-input" }} fgroup-class="col-sm-6" enable-old-support
    type="email"
    name="{{ $fieldName }}"
    label="{{ $fieldTitle }}:"
    placeholder="{{ $fieldTitle }}"
    :value="old('{{ $fieldName }}', optional(${{ $config->modelNames->camel }} ?? null)->{{ $fieldName }})"
    />
@endif