<!-- Field: {{ strtoupper($fieldName) }} -->
@if($config->options->localized)
<{{ "x-adminlte-input-file" }} fgroup-class="col-sm-6"
    name="{{ $fieldName }}"
    label="@{!! __('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}') !!}:"
    placeholder="@{!! __('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}') !!}"
    legend="@{!! __('crud.file.browse') !!}"
    />
@else
<{{ "x-adminlte-input-file" }} fgroup-class="col-sm-6"
    name="{{ $fieldName }}"
    label="{{ $fieldTitle }}:"
    placeholder="{{ $fieldTitle }}"
    legend="{{ __('crud.file.browse') }}"
    />
@endif