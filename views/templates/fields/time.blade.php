<!-- Field: {{ strtoupper($fieldName) }} -->
@@php
    ${{ $fieldName }}_config = ['format' => 'HH:mm'];
@@endphp
@if($config->options->localized)
<{{ "x-adminlte-input-date" }} fgroup-class="col-sm-6" enable-old-support
    name="{{ $fieldName }}"
    label="@{!! __('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}') !!}:"
    placeholder="@{!! __('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}') !!}"
    :config="${{ $fieldName }}_config"
    :value="old('{{ $fieldName }}', isset(${{ $config->modelNames->camel }}) ? ${{ $config->modelNames->camel }}->{{ $fieldName }} : null)"
    />
@else
<{{ "x-adminlte-input-date" }} fgroup-class="col-sm-6" enable-old-support
    name="{{ $fieldName }}"
    label="{{ $fieldTitle }}:"
    placeholder="{{ $fieldTitle }}"
    :config="${{ $fieldName }}_config"
    :value="old('{{ $fieldName }}', isset(${{ $config->modelNames->camel }}) ? ${{ $config->modelNames->camel }}->{{ $fieldName }} : null)"
    />
@endif