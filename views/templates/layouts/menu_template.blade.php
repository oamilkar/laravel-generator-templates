    [
        'resource' => '{{ $config->modelNames->camelPlural }}',
        'route'    => '{{ $config->prefixes->getRoutePrefixWith('.') }}{{ $config->modelNames->dashedPlural }}.index',
    ],