@@extends('layouts.app')

@@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2 justify-content-between">
                <div class="col-sm-6">
                    <h1>
@if($config->options->localized)
                        @@lang('models/{!! $config->modelNames->camelPlural !!}.singular') @@lang('crud.detail')
@else
                        {{ $config->modelNames->human }} Details
@endif
                    </h1>
                </div>
                <div class="col-sm-6 d-flex justify-content-end">
                @if($config->options->localized)
                    <a class="btn btn-primary mr-2" href="@{{ route('{!! $config->prefixes->getRoutePrefixWith('.') !!}{!! $config->modelNames->dashedPlural  !!}.edit', ${!! $config->modelNames->camel !!}) }}">
                        @@lang('crud.edit')
                    </a>
                    <a class="btn btn-default" href="@{{ route('{!! $config->prefixes->getRoutePrefixWith('.') !!}{!! $config->modelNames->dashedPlural  !!}.index') }}">
                        @@lang('crud.back')
                    </a>
                @else
                    <a class="btn btn-primary mr-2" href="@{{ route('{!! $config->prefixes->getRoutePrefixWith('.') !!}{!! $config->modelNames->dashedPlural  !!}.edit', ${!! $config->modelNames->camel !!}) }}">
                        Edit
                    </a>
                    <a class="btn btn-default" href="@{{ route('{!! $config->prefixes->getRoutePrefixWith('.') !!}{!! $config->modelNames->dashedPlural  !!}.index') }}">
                        Back
                    </a>
                @endif
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">
        <div class="card">

            <{{ "x-card-section" }} title="">
                <div class="row">
                    @@include('{{ $config->prefixes->getViewPrefixForInclude() }}{{ $config->modelNames->snakePlural }}.show_fields')
                </div>
            </{{ "x-card-section" }}>

        </div>
    </div>
@@endsection
