<!-- Field: {{ strtoupper($fieldName) }} -->
<div class="col-xs-12 col-sm-6">
@if($config->options->localized)
    @{!! html()->label(__('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}').':', '{{ $fieldName }}') !!}
@else
    @{!! html()->label('{{ $fieldTitle }}', '{{ $fieldName }}') !!}
@endif
    <p>@{{ ${!! $config->modelNames->camel !!}->{!! $fieldName !!} }}</p>
</div>