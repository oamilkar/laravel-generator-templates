<{{ "x-table-actions" }}
    show-url="@{{ route('{!! $config->prefixes->getRoutePrefixWith('.') !!}{!! $config->modelNames->dashedPlural !!}.show', $model->id) }}"
    edit-url="@{{ route('{!! $config->prefixes->getRoutePrefixWith('.') !!}{!! $config->modelNames->dashedPlural !!}.edit', $model->id) }}"
    delete-url="@{{ route('{!! $config->prefixes->getRoutePrefixWith('.') !!}{!! $config->modelNames->dashedPlural !!}.destroy', $model->id) }}"
>
</{{ "x-table-actions" }}>