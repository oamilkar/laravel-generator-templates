<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! html()->label('Name'), 'name' !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! html()->label('Email'), 'email' !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! html()->label('Password'), 'password' !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Confirmation Password Field -->
<div class="form-group col-sm-6">
    {!! html()->label('Password Confirmation'), 'password' !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>
