<!-- Name Field -->
<div class="col-sm-12">
    {!! html()->label('Name:'), 'name' !!}
    <p>{!! $user->name !!}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! html()->label('Email:'), 'email' !!}
    <p>{!! $user->email !!}</p>
</div>
